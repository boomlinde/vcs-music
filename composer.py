def tokenize(s):
    return s.split()

def pattern_gen(tokens):
    patterns = {}
    current_pattern = None
    for token in tokens:
        if token.endswith(':'):
            if current_pattern:
                if not current_pattern.startswith('channel'):
                    patterns[current_pattern].append(255)
            current_pattern = token[:-1]
            patterns[current_pattern] = []
        else:
            if not current_pattern.startswith('channel'):
                token = int(token, 16)
            patterns[current_pattern].append(token)
    return patterns

def volume_scale(mult, data):
    for key in data:
        if key.startswith('channel'):
            continue
        for i in range(len(data[key])):
            v = data[key][i]
            if i % 3 != 1:
                continue
            vol = int((v >> 4) * mult)
            if vol > 0xf:
                vol = 0xf
            data[key][i] = (vol << 4) | (v & 0xf)

    return data

def generate_song(data):
    for key in data:
        if not key.startswith('channel'):
            print key + ':'
            print '.byte ' + ','.join([str(x) for x in data[key]])
        else:
            print 'song%slo:' % key[-1]
            print '.byte ' + ','.join(['<' + x for x in data[key]] + ['0'])
            print 'song%shi:' % key[-1]
            print '.byte ' + ','.join(['>' + x for x in data[key]] + ['0'])

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        mult = int(sys.argv[1])
    else:
        mult = 1
    generate_song(
        volume_scale(mult,
            pattern_gen(
                tokenize(
                    sys.stdin.read()
                )
            )
        )
    )


