* Assemble with `kickass -binfile music.s`
* Samples are unsigned 8-bit 7.5-something khz before you convert them with `sampleconvert.py`
* Samples end with a (hopefully) unique byte value. This needs to be put in the `sampleend` table
* song0lo and song0hi; song1lo and song1hi are the pattern pointers for each channel. Channel loops at value 0 in this table
* Patterns are three bytes per row (interleaved) first byte is frequency (or if MSB is set, sample number), second is volume/waveform and third is note length (in ticks).
* If volume/waveform is 0, there is no new note action
* Patterns end with value $ff
