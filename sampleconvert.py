import sys

def convert(data):
    used = [False for x in range(256)]
    data = [ord(x) for x in data]
    step = 0
    smashed = []
    currbyte = 0
    for x in data:
        currbyte <<= 4
        currbyte += x >> 4
        step ^= 1
        if not step:
            smashed.append(currbyte)
            used[currbyte] = True
            currbyte = 0

    for i, x in enumerate(used):
        if not x:
            smashed.append(i)
            break

    return ''.join([chr(x) for x in smashed])


for arg in sys.argv[1:]:
    sys.stdout.write(convert(file(arg).read()))


