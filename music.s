// Atari 2600 stuff
.var  VSYNC = 0
.var VBLANK = 1
.var  WSYNC = 2
.var  AUDV = $19
.var  AUDF = $17
.var  AUDC = $15

// Sampler
.var SAMPLEPOINTER = $80
.var SAMPLESTATE = $82
.var SAMPLEPLAYING = $83
.var CURRENTSAMPLE = $84
.var CURRENTEND = $85

// Sequencer
.var CURRENTTICK = $86
.var KILL0 = $8a
.var KILL1 = $8b
.var STEP0 = $8c // Three bytes per step
.var STEP1 = $8d // Three bytes per step
.var PATTERNPOINTER0 = $8e
.var PATTERNPOINTER1 = $90
.var DOSTEP = $92
.var SONGSTEP0 = $93
.var SONGSTEP1 = $94


// Song settings
.var TICKRATE = 141
.var SPEED = 6

.pc=$f000

init:
    sei
    cld
    ldx #$ff
    txs
    inx
    lda #0
clr:
    sta 0,x
    dex
    bne clr
    ldx #$ff
    sta WSYNC

    lda song0lo
    sta PATTERNPOINTER0
    lda song0hi
    sta PATTERNPOINTER0+1

    lda song1lo
    sta PATTERNPOINTER1
    lda song1hi
    sta PATTERNPOINTER1+1

loop:
    ldx #0
wait:
    sta WSYNC
    sta WSYNC
    jsr sampletick
    inx
    cpx #TICKRATE
    bne wait
    // jmp loop
player:
    jsr seq_advance
    jmp loop

// Play the next sample. Max ~60 cycles
sampletick: {
    lda SAMPLEPLAYING
    beq done
    ldy #0
    lda (SAMPLEPOINTER),y
    cmp CURRENTEND
    beq stop
    ldy SAMPLESTATE
    cpy #0
    bne second
    lsr
    lsr
    lsr
    lsr
    jmp ok
second:
    and #$f
    inc SAMPLEPOINTER
    bne ok
    inc SAMPLEPOINTER+1
ok: 
    sta AUDV
    lda SAMPLESTATE
    eor #1
    sta SAMPLESTATE
done:
    rts
stop:
    lda #0
    sta SAMPLEPLAYING
    rts
}

// Set and trigger the sample from X register
setsample: {
    beq dontset
    lda #0
    sta SAMPLESTATE
    sta AUDC
    lda #1
    sta SAMPLEPLAYING
    lda samplelo,x
    sta SAMPLEPOINTER
    lda samplehi,x
    sta SAMPLEPOINTER+1
    lda sampleend,x
    sta CURRENTEND
    stx CURRENTSAMPLE
    lda #$ff
    sta KILL0
dontset:
    rts
}

samplelo:
    .byte 0
    .byte <kick
    .byte <snare

samplehi:
    .byte 0
    .byte >kick
    .byte >snare

sampleend:
    .byte 0
    .byte 4
    .byte 7

/////////////////////////////////////////
// SEQUENCER STUFF
/////////////////////////////////////////

incpattern0: {
    ldx SONGSTEP0
    inx 
    lda song0hi,x
    bne noreset
    ldx #0
    lda song0hi
noreset:
    sta PATTERNPOINTER0+1
    lda song0lo,x
    sta PATTERNPOINTER0
    stx SONGSTEP0
    rts
}

incpattern1: {
    ldx SONGSTEP1
    inx 
    lda song1hi,x
    bne noreset
    ldx #0
    lda song1hi
noreset:
    sta PATTERNPOINTER1+1
    lda song1lo,x
    sta PATTERNPOINTER1
    stx SONGSTEP1
    rts
}

seq_advance: {
    jsr seq_tick
    sta WSYNC
    jsr sampletick
    sta WSYNC
    lda DOSTEP
    beq nostep
    jsr advstep0
    sta WSYNC
    jsr sampletick
    sta WSYNC
    jsr advstep1
    sta WSYNC
    jsr sampletick
    sta WSYNC
nostep:
    jsr killnotes
    sta WSYNC
    jsr sampletick
    sta WSYNC
    rts
}

// Kill note of channel y
killnote:
    lda #0
    sta AUDC,y
    sta AUDV,y
    rts

killnotes: {
    lda SAMPLEPLAYING
    bne nokill0
    lda KILL0
    cmp CURRENTTICK
    bne nokill0
    lda #0
    sta AUDC
    sta AUDV
nokill0:
    lda KILL1
    cmp CURRENTTICK
    bne nokill1
    lda #0
    sta AUDC+1
    sta AUDV+1
nokill1:
    rts
}

// Increment the tick
seq_tick: {
    ldx CURRENTTICK
    bne nostep
    lda #1
    sta DOSTEP
nostep:
    inx
    cpx #SPEED
    bne noreset
    ldx #0
noreset:
    stx CURRENTTICK
    rts
}

advstep0: {
    lda #0
    sta DOSTEP
    ldy STEP0
    lda (PATTERNPOINTER0),y
    cmp #$ff
    bne noreset
    jsr incpattern0
    ldy #0
    lda (PATTERNPOINTER0),y
noreset:
    tax
    and #$80
    beq nosample
    txa
    and #$f
    tax
    jsr setsample
    iny
    iny
    iny
    sty STEP0
    rts
nosample:
    iny
    lda (PATTERNPOINTER0),y
    beq zzz
    ldx #0
    stx SAMPLEPLAYING

    tax
    lsr lsr lsr lsr
    sta AUDV

    txa
    and #$f
    sta AUDC
    dey
    lda (PATTERNPOINTER0),y
    sta AUDF
    iny
    iny
    lda (PATTERNPOINTER0),y
    sta KILL0
    iny
    sty STEP0
    rts
zzz:
    iny
    iny
    sty STEP0
    rts
}

advstep1: {
    lda #0
    sta DOSTEP
    ldy STEP1
    lda (PATTERNPOINTER1),y
    cmp #$ff
    bne noreset
    jsr incpattern1
    ldy #0
    lda (PATTERNPOINTER1),y
noreset:
    iny
    lda (PATTERNPOINTER1),y
    beq zzz
    tax
    lsr lsr lsr lsr
    sta AUDV+1

    txa
    and #$f
    sta AUDC+1
    dey
    lda (PATTERNPOINTER1),y
    sta AUDF+1
    iny
    iny
    lda (PATTERNPOINTER1),y
    sta KILL1
    iny
    sty STEP1
    rts
zzz:
    iny
    iny
    sty STEP1
    rts
}


/*
Sequencer data format
.byte freq vol/div cut
    freq - 5 bits frequency divider. msb set: set sample number
    vol/div - high nybble: 4 bit volume; low nybble: 4 bit waveform. 00 = no new note, cut still works
    cut - ticks until note is cut, but only within current line
*/


kick:
.import binary "kick.packed"
kick_end:
snare:
.import binary "snare.packed"
snare_end:

.import source "data.s"

.pc=$fffc
.word init
.word init
